from collections import defaultdict
from pprint import pprint as pp
import xmlrpclib

def main():
  client = xmlrpclib.Server("http://deepblue.mpi-inf.mpg.de/xmlrpc", allow_none=True)

  (s, v) = client.echo(None)

  version = v.split()[1][1:-1]

  ok, commands = client.commands()
  if not ok:
    print "unable to retrieve commands"
    return


  categories = defaultdict(list)
  categories_descr = {}

  commands_long_doc = ""

  for name in sorted(commands.keys()):
    
    cmd = commands[name]
    description = cmd["description"]

    category = description[0]
    category_description = description[1]
    description_text = description[2]

    categories_descr[category] = category_description
    
    parameters = cmd["parameters"]
    results = cmd['results']

    cmd = {"name": name.replace("_", "\_"),
                "parameters": parameters,
                "results": results,
                "description": description_text.replace("_", "\_").replace("&", "\&")}                          

    categories[category].append(cmd)

  template = ""
  with open("deepblue_api.tex", 'w') as f:
    f.write(template)

  print "\subsection{API}"
  print
  print "Here the entire DeepBlue API is listed:"
  print "TODO: include admin commands"
  print

  for category in categories:    
    print """\\subsubsection{%(category)s} 
\label{sec:app:api:%(category)s}
{\\footnotesize %(category_description)s}

""" % {
      "category": category, 
      "category_description": categories_descr[category]}    

    commands = categories[category]
    for command in commands:
      print "\\normalsize"
      print "\\textit{\\textbf{"+command["name"] + "}(" + ",".join([param[0].replace("_", "\_") for param in command["parameters"]]) + ")}" 
      print "\\label{sec:app:api:cmd:" + command["name"] + "}"
      print
      print "\\footnotesize"
      print command["description"]      
      print

if __name__ == "__main__":
  main()
